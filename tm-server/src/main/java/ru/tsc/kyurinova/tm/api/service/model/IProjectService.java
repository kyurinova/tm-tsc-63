package ru.tsc.kyurinova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;

import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    void addAll(@NotNull List<Project> projects);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project findByName(@Nullable String userId, @Nullable String name);

    void removeByName(@Nullable String userId, @Nullable String name);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    void startById(@Nullable String userId, @Nullable String id);

    void startByIndex(@Nullable String userId, @Nullable Integer index);

    void startByName(@Nullable String userId, @Nullable String name);

    void finishById(@Nullable String userId, @Nullable String id);

    void finishByIndex(@Nullable String userId, @Nullable Integer index);

    void finishByName(@Nullable String userId, @Nullable String name);

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
